program Pcfc;

uses
  Vcl.Forms,
  Unit1 in '..\source\Unit1.pas' {Fmain},
  Udet in '..\source\Udet.pas' {Fdet},
  Udm in '..\source\Udm.pas' {DM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFmain, Fmain);
  Application.CreateForm(TFdet, Fdet);
  Application.Run;
end.
