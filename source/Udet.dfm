object Fdet: TFdet
  Left = 0
  Top = 0
  Caption = 'Fdet'
  ClientHeight = 618
  ClientWidth = 836
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 836
    Height = 201
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 57
      Width = 26
      Height = 13
      Caption = ' Tipo '
      Layout = tlCenter
    end
    object Label2: TLabel
      Left = 238
      Top = 59
      Width = 98
      Height = 13
      Caption = ' Anni ammortamenti '
      Layout = tlCenter
    end
    object Label3: TLabel
      Left = 150
      Top = 59
      Width = 80
      Height = 13
      Caption = ' Importo in Euro '
      Layout = tlCenter
    end
    object Label4: TLabel
      Left = 345
      Top = 59
      Width = 55
      Height = 13
      Caption = ' Date inizio '
      Layout = tlCenter
    end
    object Label5: TLabel
      Left = 149
      Top = 9
      Width = 119
      Height = 13
      Caption = 'Descrizione registrazione'
      Layout = tlCenter
    end
    object Label9: TLabel
      Left = 22
      Top = 9
      Width = 47
      Height = 13
      Caption = 'Protocollo'
      Layout = tlCenter
    end
    object GroupBox1: TGroupBox
      Left = 20
      Top = 101
      Width = 552
      Height = 87
      Caption = 'Protocollo fornitore'
      TabOrder = 0
      object Label6: TLabel
        Left = 15
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Fornitore'
        Layout = tlCenter
      end
      object Label7: TLabel
        Left = 14
        Top = 48
        Width = 54
        Height = 13
        Caption = 'Documento'
        Layout = tlCenter
      end
      object Label8: TLabel
        Left = 231
        Top = 48
        Width = 23
        Height = 13
        Caption = 'Data'
        Layout = tlCenter
      end
      object RzDBEdit1: TRzDBEdit
        Left = 444
        Top = 20
        Width = 66
        Height = 21
        DataSource = DSTbldoc
        DataField = 'cod_spp'
        ReadOnly = True
        Color = clInfoBk
        TabOrder = 0
      end
      object RzDBEdit6: TRzDBEdit
        Left = 74
        Top = 45
        Width = 152
        Height = 21
        DataSource = DSTbldoc
        DataField = 'num_inv'
        Enabled = False
        TabOrder = 1
      end
      object RzDBDateTimeEdit2: TRzDBDateTimeEdit
        Left = 260
        Top = 45
        Width = 178
        Height = 21
        DataSource = DSTbldoc
        DataField = 'dta_inv'
        Enabled = False
        TabOrder = 2
        EditType = etDate
      end
      object RzDBLookupComboBox1: TRzDBLookupComboBox
        Left = 73
        Top = 18
        Width = 365
        Height = 21
        DataField = 'nam_spp'
        DataSource = DSTbldoc
        Enabled = False
        KeyField = 'nam_spp'
        ListField = 'nam_spp'
        ListSource = DSTblspp
        TabOrder = 3
      end
    end
    object RzDBEdit2: TRzDBEdit
      Left = 21
      Top = 28
      Width = 121
      Height = 21
      DataSource = DSTbldoc
      DataField = 'protocollo'
      ReadOnly = True
      Color = clInfoBk
      TabOrder = 1
    end
    object Vdesc: TRzDBEdit
      Left = 148
      Top = 28
      Width = 424
      Height = 21
      DataSource = DSTbldoc
      DataField = 'nam_reg'
      TabOrder = 2
    end
    object RzDBComboBox1: TRzDBComboBox
      Left = 22
      Top = 74
      Width = 120
      Height = 21
      DataField = 'tipo'
      DataSource = DSTbldoc
      Style = csDropDownList
      TabOrder = 3
      OnChange = RzDBComboBox1Change
      Items.Strings = (
        'cespite'
        'costo'
        'ricavo')
    end
    object RzDBEdit4: TRzDBEdit
      Left = 148
      Top = 74
      Width = 80
      Height = 21
      DataSource = DSTbldoc
      DataField = 'importo'
      TabOrder = 4
    end
    object RzDBDateTimeEdit1: TRzDBDateTimeEdit
      Left = 345
      Top = 74
      Width = 121
      Height = 21
      DataSource = DSTbldoc
      DataField = 'dta_start'
      TabOrder = 5
      EditType = etDate
    end
    object Vimporto: TRzDBEdit
      Left = 253
      Top = 74
      Width = 80
      Height = 21
      DataSource = DSTbldoc
      DataField = 'year_reg'
      TabOrder = 6
    end
    object GroupBox2: TGroupBox
      Left = 578
      Top = 22
      Width = 241
      Height = 166
      Caption = 'Scadenze pagamento'
      TabOrder = 7
      object Vsca1: TRzDateTimeEdit
        Left = 16
        Top = 26
        Width = 121
        Height = 21
        EditType = etDate
        Enabled = False
        ImeName = 'Vsca'
        TabOrder = 0
      end
      object Vsca2: TRzDateTimeEdit
        Left = 16
        Top = 53
        Width = 121
        Height = 21
        EditType = etDate
        Enabled = False
        ImeName = 'Vsca'
        TabOrder = 1
      end
      object Vsca3: TRzDateTimeEdit
        Left = 16
        Top = 80
        Width = 121
        Height = 21
        EditType = etDate
        Enabled = False
        ImeName = 'Vsca'
        TabOrder = 2
      end
      object Vsca4: TRzDateTimeEdit
        Left = 16
        Top = 107
        Width = 121
        Height = 21
        EditType = etDate
        Enabled = False
        ImeName = 'Vsca'
        TabOrder = 3
      end
      object Vsca5: TRzDateTimeEdit
        Left = 16
        Top = 133
        Width = 121
        Height = 21
        EditType = etDate
        Enabled = False
        ImeName = 'Vsca'
        TabOrder = 4
      end
      object Vimp1: TRzNumericEdit
        Left = 143
        Top = 26
        Width = 95
        Height = 21
        Enabled = False
        TabOrder = 5
        DisplayFormat = ',0;(,0)'
      end
      object Vimp2: TRzNumericEdit
        Left = 143
        Top = 53
        Width = 95
        Height = 21
        Enabled = False
        TabOrder = 6
        DisplayFormat = ',0;(,0)'
      end
      object Vimp3: TRzNumericEdit
        Left = 143
        Top = 80
        Width = 95
        Height = 21
        Enabled = False
        TabOrder = 7
        DisplayFormat = ',0;(,0)'
      end
      object Vimp4: TRzNumericEdit
        Left = 143
        Top = 107
        Width = 95
        Height = 21
        Enabled = False
        TabOrder = 8
        DisplayFormat = ',0;(,0)'
      end
      object Vimp5: TRzNumericEdit
        Left = 143
        Top = 133
        Width = 95
        Height = 21
        Enabled = False
        TabOrder = 9
        DisplayFormat = ',0;(,0)'
      end
    end
  end
  object Pscelta: TPanel
    Left = 0
    Top = 201
    Width = 836
    Height = 417
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 834
      Height = 374
      Align = alClient
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = -1
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DSTblPDC
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
        object G1perc: TcxGridDBColumn
          Caption = 'Percentuale'
          DataBinding.FieldName = 'perc'
          Styles.OnGetContentStyle = G1percStylesGetContentStyle
        end
        object cxGrid1DBTableView1mastro: TcxGridDBColumn
          DataBinding.FieldName = 'mastro'
        end
        object G1conto: TcxGridDBColumn
          DataBinding.FieldName = 'conto'
        end
        object cxGrid1DBTableView1sottoconto: TcxGridDBColumn
          DataBinding.FieldName = 'sottoconto'
        end
        object cxGrid1DBTableView1descrizione: TcxGridDBColumn
          DataBinding.FieldName = 'descrizione'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 375
      Width = 834
      Height = 41
      Align = alBottom
      TabOrder = 1
      object Button2: TButton
        AlignWithMargins = True
        Left = 6
        Top = 6
        Width = 75
        Height = 29
        Margins.Left = 5
        Margins.Top = 5
        Margins.Right = 5
        Margins.Bottom = 5
        Align = alLeft
        Caption = 'OK'
        TabOrder = 0
        OnClick = Button2Click
      end
    end
  end
  object TblPDC: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 704
    Top = 312
    object TblPDCscelta: TBooleanField
      FieldName = 'scelta'
    end
    object TblPDCperc: TFloatField
      FieldName = 'perc'
    end
    object TblPDCmastro: TStringField
      FieldName = 'mastro'
      Size = 10
    end
    object TblPDCconto: TStringField
      FieldName = 'conto'
      Size = 10
    end
    object TblPDCsottoconto: TStringField
      FieldName = 'sottoconto'
      Size = 10
    end
    object TblPDCdescrizione: TStringField
      FieldName = 'descrizione'
      Size = 100
    end
    object TblPDCtipo: TStringField
      FieldName = 'tipo'
      Size = 1
    end
  end
  object DSTblPDC: TDataSource
    DataSet = TblPDC
    Left = 600
    Top = 264
  end
  object Tblnew: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 536
    Top = 320
    object TblnewTipo: TStringField
      FieldName = 'Tipo'
    end
    object Tblnewmastro: TStringField
      FieldName = 'mastro'
      Size = 10
    end
    object Tblnewconto: TStringField
      FieldName = 'conto'
      Size = 10
    end
    object Tblnewsottoconto: TStringField
      FieldName = 'sottoconto'
      Size = 10
    end
    object Tblnewimporto: TFloatField
      FieldName = 'importo'
    end
    object Tblnewdata: TDateField
      FieldName = 'data'
    end
    object Tblnewperc: TFloatField
      FieldName = 'perc'
    end
    object Tblnewname: TStringField
      FieldName = 'name'
      Size = 100
    end
  end
  object DSTblnew: TDataSource
    DataSet = Tblnew
    Left = 608
    Top = 320
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 704
    Top = 264
    PixelsPerInch = 96
    object StyleGrassetto: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      TextColor = clLime
    end
    object StyleStd: TcxStyle
    end
    object StyleMastro: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clAqua
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object TblREG: TFDTable
    IndexFieldNames = 'mastro;conto;sottoconto'
    UpdateOptions.UpdateTableName = 'nglarc_cfc_reg'
    TableName = 'nglarc_cfc_reg'
    Left = 533
    Top = 257
    object TblREGazienda: TIntegerField
      FieldName = 'azienda'
      Origin = 'azienda'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGtipo: TStringField
      FieldName = 'tipo'
      Origin = 'tipo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGmastro: TStringField
      FieldName = 'mastro'
      Origin = 'mastro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGconto: TStringField
      FieldName = 'conto'
      Origin = 'conto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGsottoconto: TStringField
      FieldName = 'sottoconto'
      Origin = 'sottoconto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGdata: TDateField
      FieldName = 'data'
      Origin = 'data'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGton: TBCDField
      FieldName = 'ton'
      Origin = 'ton'
      Precision = 15
      Size = 3
    end
    object TblREGdescrizione: TStringField
      FieldName = 'descrizione'
      Origin = 'descrizione'
      Size = 100
    end
    object TblREGid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ReadOnly = True
    end
    object TblREGprotocollo: TStringField
      FieldName = 'protocollo'
      Origin = 'protocollo'
    end
    object TblREGimporto: TBCDField
      FieldName = 'importo'
      Origin = 'importo'
      Required = True
      Precision = 10
      Size = 2
    end
  end
  object TblDoc: TFDTable
    UpdateOptions.UpdateTableName = 'nglarc_cfc_regt'
    UpdateOptions.KeyFields = 'id'
    TableName = 'nglarc_cfc_regt'
    Left = 530
    Top = 383
    object TblDocprotocollo: TStringField
      FieldName = 'protocollo'
      Origin = 'protocollo'
    end
    object TblDoctipo: TStringField
      FieldName = 'tipo'
      Origin = 'tipo'
    end
    object TblDocimporto: TBCDField
      FieldName = 'importo'
      Origin = 'importo'
      Precision = 10
      Size = 2
    end
    object TblDocdta_start: TDateField
      FieldName = 'dta_start'
      Origin = 'dta_start'
      Required = True
    end
    object TblDocyear_reg: TIntegerField
      FieldName = 'year_reg'
      Origin = 'year_reg'
    end
    object TblDocnam_spp: TStringField
      FieldName = 'nam_spp'
      Origin = 'nam_spp'
      Size = 100
    end
    object TblDocnum_inv: TStringField
      FieldName = 'num_inv'
      Origin = 'num_inv'
    end
    object TblDocdta_inv: TDateField
      FieldName = 'dta_inv'
      Origin = 'dta_inv'
    end
    object TblDocnam_reg: TStringField
      FieldName = 'nam_reg'
      Origin = 'nam_reg'
      Size = 100
    end
    object TblDoccod_spp: TStringField
      FieldName = 'cod_spp'
      Origin = 'cod_spp'
      Size = 6
    end
    object TblDocid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ReadOnly = True
    end
  end
  object DSTbldoc: TDataSource
    DataSet = TblDoc
    Left = 776
    Top = 320
  end
  object TBlsca: TFDTable
    UpdateOptions.UpdateTableName = 'nglarc_cfc_regs'
    TableName = 'nglarc_cfc_regs'
    Left = 706
    Top = 375
    object TBlscaid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ReadOnly = True
    end
    object TBlscaprotocollo: TStringField
      FieldName = 'protocollo'
      Origin = 'protocollo'
    end
    object TBlscascadenza: TDateField
      FieldName = 'scadenza'
      Origin = 'scadenza'
    end
    object TBlscaimporto: TBCDField
      FieldName = 'importo'
      Origin = 'importo'
      Precision = 10
      Size = 2
    end
  end
  object ActionList1: TActionList
    Left = 528
    Top = 441
    object a_salva: TAction
      Caption = 'a_salva'
      OnExecute = a_salvaExecute
    end
  end
  object DSTblspp: TDataSource
    DataSet = DM.Tblspp
    Left = 464
    Top = 329
  end
end
