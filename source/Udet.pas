unit Udet;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Mask, System.UITypes,
  RzEdit, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinBlue, dxSkinVisualStudio2013Blue, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,math, Vcl.ComCtrls,dateutils, FireDAC.Stan.Async,
  FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait,
  RzDBEdit, RzCmboBx, RzDBCmbo, System.Actions, Vcl.ActnList, Vcl.DBCtrls,
  dxSkinBlack, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, System.StrUtils;

type
  TFdet = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Pscelta: TPanel;
    TblPDC: TFDMemTable;
    TblPDCscelta: TBooleanField;
    TblPDCmastro: TStringField;
    TblPDCconto: TStringField;
    TblPDCdescrizione: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel3: TPanel;
    Button2: TButton;
    DSTblPDC: TDataSource;
    cxGrid1DBTableView1mastro: TcxGridDBColumn;
    G1conto: TcxGridDBColumn;
    cxGrid1DBTableView1descrizione: TcxGridDBColumn;
    TblPDCperc: TFloatField;
    G1perc: TcxGridDBColumn;
    Tblnew: TFDMemTable;
    TblnewTipo: TStringField;
    Tblnewmastro: TStringField;
    Tblnewconto: TStringField;
    Tblnewsottoconto: TStringField;
    Tblnewimporto: TFloatField;
    Tblnewdata: TDateField;
    DSTblnew: TDataSource;
    TblPDCsottoconto: TStringField;
    cxGrid1DBTableView1sottoconto: TcxGridDBColumn;
    Label4: TLabel;
    Tblnewperc: TFloatField;
    Label5: TLabel;
    Tblnewname: TStringField;
    cxStyleRepository1: TcxStyleRepository;
    StyleGrassetto: TcxStyle;
    StyleStd: TcxStyle;
    TblREG: TFDTable;
    TblREGazienda: TIntegerField;
    TblREGtipo: TStringField;
    TblREGmastro: TStringField;
    TblREGconto: TStringField;
    TblREGsottoconto: TStringField;
    TblREGdata: TDateField;
    TblREGton: TBCDField;
    TblREGdescrizione: TStringField;
    TblREGid: TFDAutoIncField;
    TblREGprotocollo: TStringField;
    StyleMastro: TcxStyle;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    TblDoc: TFDTable;
    TblDocprotocollo: TStringField;
    RzDBEdit1: TRzDBEdit;
    DSTbldoc: TDataSource;
    Label9: TLabel;
    RzDBEdit2: TRzDBEdit;
    Vdesc: TRzDBEdit;
    RzDBComboBox1: TRzDBComboBox;
    TblDoctipo: TStringField;
    RzDBEdit4: TRzDBEdit;
    RzDBDateTimeEdit1: TRzDBDateTimeEdit;
    TblDocimporto: TBCDField;
    TblDocdta_start: TDateField;
    Vimporto: TRzDBEdit;
    TblDocyear_reg: TIntegerField;
    TblDocnam_spp: TStringField;
    RzDBEdit6: TRzDBEdit;
    RzDBDateTimeEdit2: TRzDBDateTimeEdit;
    TblDocnum_inv: TStringField;
    TblDocdta_inv: TDateField;
    TblDocnam_reg: TStringField;
    GroupBox2: TGroupBox;
    TBlsca: TFDTable;
    Vsca1: TRzDateTimeEdit;
    Vsca2: TRzDateTimeEdit;
    Vsca3: TRzDateTimeEdit;
    Vsca4: TRzDateTimeEdit;
    Vsca5: TRzDateTimeEdit;
    Vimp1: TRzNumericEdit;
    Vimp2: TRzNumericEdit;
    Vimp3: TRzNumericEdit;
    Vimp4: TRzNumericEdit;
    Vimp5: TRzNumericEdit;
    ActionList1: TActionList;
    a_salva: TAction;
    TblDoccod_spp: TStringField;
    TblREGimporto: TBCDField;
    TBlscaid: TFDAutoIncField;
    TBlscaprotocollo: TStringField;
    TBlscascadenza: TDateField;
    TBlscaimporto: TBCDField;
    RzDBLookupComboBox1: TRzDBLookupComboBox;
    DSTblspp: TDataSource;
    TblDocid: TFDAutoIncField;
    TblPDCtipo: TStringField;
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure G1percStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure a_salvaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure RzDBComboBox1Change(Sender: TObject);
  private       { Private declarations }

       procedure azz_scadenze;
       procedure enable_state_costo(state : boolean);

  public
    { Public declarations }
  end;

var
  Fdet: TFdet;

implementation

{$R *.dfm}

uses Udm;

procedure TFdet.a_salvaExecute(Sender: TObject);
begin
  //
  // Crea Registrazione
  // ===>>
  if MessageDlg('Creare le registrazioni ?', mtConfirmation, [mbYes, mbNo], 0, mbYes) = mrNo then
  begin
      Abort;
  end;
  // ===>>
  tblreg.Close;
  tblreg.open;
  tblsca.Close;
  tblsca.open;
  if trim(TblDocnam_spp.AsString)<>'' then
  begin
    tbldoc.Edit;
    TblDoccod_spp.AsString:=dm.Tblsppcod_spp.AsString;
    tbldoc.Post;
  end;
  tblnew.First;
  while not tblnew.eof do
  begin
    // -->
    Tblreg.append;
    TblREGprotocollo.AsString   := TblDocprotocollo.AsString;
    TblREGazienda.AsInteger     := 1;
    TblregTipo.AsString         := TblnewTipo.AsString;
    TblREGdescrizione.AsString  := Tblnewname.AsString;
    TblREGmastro.AsString       := Tblnewmastro.AsString;
    TblREGconto.AsString        := Tblnewconto.AsString;
    TblREGsottoconto.AsString   := Tblnewsottoconto.AsString;
    TblREGdata.AsDateTime       := Tblnewdata.AsDateTime;
    TblREGimporto.AsFloat       := Tblnewimporto.AsFloat;
    TblREGton.AsFloat           := 0;
    TblREG.Post;
    tblnew.Next;
  end;
  tblnew.First;
  // Crea scadenza
  if Vimp1.Value<>0 then
  begin
    tblsca.Append;
    TBlscaprotocollo.asstring := TblDocprotocollo.AsString;
    TBlscascadenza.AsDateTime := Vsca1.Date;
    TBlscaimporto.AsFloat     := Vimp1.Value;
    tblsca.Post
  end;
  if Vimp2.Value<>0 then
  begin
    tblsca.Append;
    TBlscaprotocollo.asstring:= TblDocprotocollo.AsString;
    TBlscascadenza.AsDateTime:= Vsca2.Date;
    TBlscaimporto.AsFloat     := Vimp2.Value;
    tblsca.Post
  end;
  if Vimp3.Value<>0 then
  begin
    tblsca.Append;
    TBlscaprotocollo.asstring:= TblDocprotocollo.AsString;
    TBlscascadenza.AsDateTime:= Vsca3.Date;
    TBlscaimporto.AsFloat     := Vimp3.Value;
    tblsca.Post
  end;
  if Vimp4.Value<>0 then
  begin
    tblsca.Append;
    TBlscaprotocollo.asstring:= TblDocprotocollo.AsString;
    TBlscascadenza.AsDateTime:= Vsca4.Date;
    TBlscaimporto.AsFloat     := Vimp4.Value;
    tblsca.Post
  end;
  if Vimp5.Value<>0 then
  begin
    tblsca.Append;
    TBlscaprotocollo.asstring:= TblDocprotocollo.AsString;
    TBlscascadenza.AsDateTime:= Vsca5.Date;
    TBlscaimporto.AsFloat     := Vimp5.Value;
    tblsca.Post
  end;

  azz_scadenze;
  close;
  // ===>>  //

end;

procedure TFdet.Button2Click(Sender: TObject);
var
  VQuota,Wanni : extended ;
  NumeroPeriodi,i : integer;
  impScad  : Double;
begin
  if RzDBComboBox1.ItemIndex < 0 then
  begin
       Showmessage('Selezionare il Tipo Registrazione!');
       RzDBComboBox1.SetFocus;
       abort;
  end;
  if Trim(TblDocnam_reg.Text)='' then
  begin
    Showmessage('Non hai inserito la descrizione!');
    Vdesc.SetFocus;
    abort;
  end;
  if TblDocimporto.Value<=0 then
  begin
    Showmessage('Inserire l''importo !');
    Vimporto.SetFocus;
    abort;
  end;
  if RzDBComboBox1.ItemIndex <> 2 then
  begin
       impScad := Vimp1.Value + Vimp2.Value + Vimp3.Value + Vimp4.Value + Vimp5.Value;
       if TblDocimporto.Value <> impScad then
       begin
            ShowMessage('Le Scadenze Pagamento Devono essere uguali all''Importo in Euro');
            Vimp1.SetFocus;
            Abort;
       end;
  end;
  // Controllo che percentuale <= 100
  if (tbldoc.State=dsEdit) or (tbldoc.State=dsInsert) then
  begin
    tbldoc.Post;
  end;
  tblpdc.First;
  Vquota:=0;
  while not tblpdc.eof do
  begin
    if TblPDCperc.AsFloat<>0  then  Vquota := Vquota + TblPDCperc.AsFloat;
    tblpdc.Next;
  end;
  if Vquota <> 100 then
  begin
    showmessage('La Percentuale � diversa da 100 %');
    abort;
  end;
  //
  tblpdc.First;
  while not tblpdc.eof do
  begin
    if TblPDCperc.AsFloat<>0  then
    begin
      Vquota      := TblDocimporto.Value * (TblPDCperc.asfloat/100);
      if TblDocyear_reg.Value=0 then
      begin
        Wanni      := 1 ;
      end
      else
      begin
        Wanni      := (TblDocyear_reg.value * 12) ;
      end;
      NumeroPeriodi  := round(Wanni);
      Vquota      := Roundto( (Vquota/NumeroPeriodi) ,-2);
      for i  := 1 to numeroperiodi do
      begin
        Tblnew.append;
        TblnewTipo.AsString       := TblDoctipo.AsString;   // Vtipo.Text;
        Tblnewmastro.AsString     := TblPDCmastro.AsString;
        Tblnewconto.AsString      := TblPDCconto.AsString;
        Tblnewsottoconto.AsString := TblPDCsottoconto.AsString;
        Tblnewname.AsString       := Vdesc.Text;
        Tblnewdata.AsDateTime     := incmonth(TblDocdta_start.Value,i-1);
        Tblnewimporto.AsFloat     := Vquota;
        Tblnew.Post;
      end;
    end;
    tblpdc.Next;
  end;
  a_salva.Execute;

  //
end;

procedure TFdet.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
     s      : string;
     i,
     lConto : integer;
     cZero  : boolean;
begin
     if not DM.applyStyle then exit;

     s      := ARecord.Values[cxGrid1DBTableView1descrizione.Index];
     s      := Trim(ARecord.Values[G1Conto.Index]);
     lConto := Length(s);
     if lConto < 1 then
     begin
          AStyle := StyleStd;
          exit;
     end;
     i := 1;
     repeat
           cZero := (MidStr(s, i, 1) = '0');
           Inc(i);
     until (i > lConto) or (not cZero);

     if cZero then
        AStyle := StyleMastro
     else
        AStyle := StyleStd;
end;

procedure TFdet.FormShow(Sender: TObject);
begin
  // Esplosione cespiti
  caption:='Esplosione cespiti';
  enable_state_costo(False);
  TblPDC.Filter   := '';
  TblPDC.Filtered := False;
  tblnew.Active   := TRue;
  tblnew.EmptyDataSet;
  //
  Vdesc.SetFocus;
  // fornitore costo alla scadenza unica piu 5 rate  + protocollo del documento + tabelle fornitore

end;

procedure TFdet.G1percStylesGetContentStyle(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  var AStyle: TcxStyle);
begin
   if ARecord.Values[G1perc.Index] > 0  then
    AStyle := StyleGrassetto
  else
    AStyle := StyleStd;
end;

procedure TFdet.RzDBComboBox1Change(Sender: TObject);
var
     s : string;
begin
     enable_state_costo(RzDBComboBox1.ItemIndex <> 2);
     if RzDBComboBox1.ItemIndex = 2 then
        s := 'R'
     else
        s := 'C';

     TblPDC.Filter   := 'tipo = ' + QuotedStr(s);
     TblPDC.Filtered := True;
end;

procedure TFdet.azz_scadenze;
begin
     Vsca1.Date  := 0;
     Vsca2.Date  := 0;
     Vsca3.Date  := 0;
     Vsca4.Date  := 0;
     Vsca5.Date  := 0;
     Vimp1.Value := 0;
     Vimp2.Value := 0;
     Vimp3.Value := 0;
     Vimp4.Value := 0;
     Vimp5.Value := 0;
end;

procedure TFDet.enable_state_costo(state: Boolean);
begin
     RzDBLookupComboBox1.Enabled := state;
     RzDBEdit6.Enabled           := state;
     RzDBDateTimeEdit2.Enabled   := state;
     Vsca1.Enabled               := state;
     Vsca2.Enabled               := state;
     Vsca3.Enabled               := state;
     Vsca4.Enabled               := state;
     Vsca5.Enabled               := state;
     Vimp1.Enabled               := state;
     Vimp2.Enabled               := state;
     Vimp3.Enabled               := state;
     Vimp4.Enabled               := state;
     Vimp5.Enabled               := state;
end;

end.
