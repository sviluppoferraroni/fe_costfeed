object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 327
  Width = 404
  object DB: TFDConnection
    Params.Strings = (
      'Database=dbferraroni'
      'User_Name=sa'
      'Password=FerroSeF+++'
      'Server=SERVER2\SISTEMI'
      'DriverID=MSSQL')
    Left = 84
    Top = 45
  end
  object Tblspp: TFDTable
    Connection = DB
    UpdateOptions.UpdateTableName = 'nglarc_cfc_spp'
    TableName = 'nglarc_cfc_spp'
    Left = 140
    Top = 48
    object Tblsppid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ReadOnly = True
    end
    object Tblsppcod_spp: TStringField
      DisplayLabel = 'Code'
      FieldName = 'cod_spp'
      Origin = 'cod_spp'
      Required = True
      Size = 6
    end
    object Tblsppnam_spp: TStringField
      DisplayLabel = 'Descrizione'
      FieldName = 'nam_spp'
      Origin = 'nam_spp'
      Required = True
      Size = 100
    end
  end
  object TblREG: TFDTable
    IndexFieldNames = 'protocollo;mastro;conto;sottoconto'
    Connection = DB
    UpdateOptions.UpdateTableName = 'nglarc_cfc_reg'
    TableName = 'nglarc_cfc_reg'
    Left = 140
    Top = 105
    object TblREGazienda: TIntegerField
      DisplayLabel = 'Azienda'
      FieldName = 'azienda'
      Origin = 'azienda'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGtipo: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'tipo'
      Origin = 'tipo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGmastro: TStringField
      DisplayLabel = 'Mastro'
      FieldName = 'mastro'
      Origin = 'mastro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGconto: TStringField
      DisplayLabel = 'Conto'
      FieldName = 'conto'
      Origin = 'conto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGsottoconto: TStringField
      DisplayLabel = 'Sottoconto'
      FieldName = 'sottoconto'
      Origin = 'sottoconto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblREGdata: TDateField
      DisplayLabel = 'Data'
      FieldName = 'data'
      Origin = 'data'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object TblREGton: TBCDField
      FieldName = 'ton'
      Origin = 'ton'
      Precision = 15
      Size = 3
    end
    object TblREGdescrizione: TStringField
      DisplayLabel = 'Descrizione'
      FieldName = 'descrizione'
      Origin = 'descrizione'
      Size = 100
    end
    object TblREGid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ReadOnly = True
    end
    object TblREGprotocollo: TStringField
      DisplayLabel = 'Protocollo'
      FieldName = 'protocollo'
      Origin = 'protocollo'
    end
    object TblREGimporto: TBCDField
      FieldName = 'importo'
      Origin = 'importo'
      Required = True
      Precision = 10
      Size = 2
    end
  end
  object TblPDC: TFDTable
    IndexFieldNames = 'mastro;conto;sottoconto'
    Connection = DB
    UpdateOptions.UpdateTableName = 'dbferraroni.dbo.nglarc_cfc_pdc'
    UpdateOptions.KeyFields = 'id'
    TableName = 'dbferraroni.dbo.nglarc_cfc_pdc'
    Left = 84
    Top = 105
    object TblPDCmastro: TStringField
      FieldName = 'mastro'
      Origin = 'mastro'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblPDCconto: TStringField
      FieldName = 'conto'
      Origin = 'conto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblPDCsottoconto: TStringField
      FieldName = 'sottoconto'
      Origin = 'sottoconto'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object TblPDCdescrizione: TStringField
      FieldName = 'descrizione'
      Origin = 'descrizione'
      Size = 100
    end
    object TblPDCtipo: TStringField
      FieldName = 'tipo'
      Origin = 'tipo'
      Size = 1
    end
    object TblPDCfg_produzione: TStringField
      FieldName = 'fg_produzione'
      Origin = 'fg_produzione'
      Size = 1
    end
    object TblPDCid: TFDAutoIncField
      FieldName = 'id'
      Origin = 'id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
  end
end
