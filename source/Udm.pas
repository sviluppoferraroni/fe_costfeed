unit Udm;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TDM = class(TDataModule)
    DB: TFDConnection;
    Tblspp: TFDTable;
    Tblsppid: TFDAutoIncField;
    Tblsppcod_spp: TStringField;
    Tblsppnam_spp: TStringField;
    TblREG: TFDTable;
    TblREGazienda: TIntegerField;
    TblREGtipo: TStringField;
    TblREGmastro: TStringField;
    TblREGconto: TStringField;
    TblREGsottoconto: TStringField;
    TblREGdata: TDateField;
    TblREGton: TBCDField;
    TblREGdescrizione: TStringField;
    TblREGid: TFDAutoIncField;
    TblREGprotocollo: TStringField;
    TblREGimporto: TBCDField;
    TblPDC: TFDTable;
    TblPDCmastro: TStringField;
    TblPDCconto: TStringField;
    TblPDCsottoconto: TStringField;
    TblPDCdescrizione: TStringField;
    TblPDCtipo: TStringField;
    TblPDCfg_produzione: TStringField;
    TblPDCid: TFDAutoIncField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private       { Private declarations }

  public        { Public declarations }
  var
       applyStyle : boolean;

  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  TblPDC.Open;
  TblREG.Open;
  Tblspp.Open;
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  TblPDC.close;
  TblREG.close;
  Tblspp.close;
  DB.Close;
end;

end.
