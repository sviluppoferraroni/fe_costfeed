object Fmain: TFmain
  Left = 0
  Top = 0
  ActiveControl = PG1
  Caption = 'Fmain'
  ClientHeight = 623
  ClientWidth = 828
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PG1: TPageControl
    Left = 0
    Top = 0
    Width = 828
    Height = 623
    ActivePage = Tabmain
    Align = alClient
    TabOrder = 0
    object Tabmain: TTabSheet
      Caption = 'Dati'
      object cxGrid2: TcxGrid
        Left = 0
        Top = 41
        Width = 820
        Height = 554
        Align = alClient
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DSTblREG
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid2DBTableView1protocollo: TcxGridDBColumn
            DataBinding.FieldName = 'protocollo'
          end
          object cxGrid2DBTableView1descrizione: TcxGridDBColumn
            Caption = 'Descrizione'
            DataBinding.FieldName = 'descrizione'
            Width = 296
          end
          object cxGrid2DBTableView1tipo: TcxGridDBColumn
            DataBinding.FieldName = 'tipo'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Items.Strings = (
              'cespite'
              'costo'
              'ricavo')
          end
          object cxGrid2DBTableView1mastro: TcxGridDBColumn
            DataBinding.FieldName = 'mastro'
          end
          object cxGrid2DBTableView1conto: TcxGridDBColumn
            DataBinding.FieldName = 'conto'
          end
          object cxGrid2DBTableView1sottoconto: TcxGridDBColumn
            DataBinding.FieldName = 'sottoconto'
          end
          object cxGrid2DBTableView1data: TcxGridDBColumn
            DataBinding.FieldName = 'data'
          end
          object cxGrid2DBTableView1importo: TcxGridDBColumn
            DataBinding.FieldName = 'importo'
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 820
        Height = 41
        Align = alTop
        TabOrder = 1
        object Bcespite: TButton
          AlignWithMargins = True
          Left = 6
          Top = 6
          Width = 123
          Height = 29
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alLeft
          Caption = 'Creazione Costi/Ricavi'
          TabOrder = 0
          OnClick = BcespiteClick
        end
      end
    end
    object TabPDC: TTabSheet
      Caption = 'Piano dei conti'
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 820
        Height = 595
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          DataController.DataSource = DSTblPDC
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid1DBTableView1mastro: TcxGridDBColumn
            Caption = 'Mastro'
            DataBinding.FieldName = 'mastro'
          end
          object cxGrid1DBTableView1conto: TcxGridDBColumn
            Caption = 'Conto'
            DataBinding.FieldName = 'conto'
          end
          object cxGrid1DBTableView1sottoconto: TcxGridDBColumn
            Caption = 'Sotto Conto'
            DataBinding.FieldName = 'sottoconto'
          end
          object cxGrid1DBTableView1descrizione: TcxGridDBColumn
            Caption = 'Descrizione'
            DataBinding.FieldName = 'descrizione'
            Width = 355
          end
          object cxGrid1DBTableView1tipo: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'tipo'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Items.Strings = (
              'C'
              'R')
            Width = 52
          end
          object cxGrid1DBTableView1fg_produzione: TcxGridDBColumn
            Caption = 'Produzione'
            DataBinding.FieldName = 'fg_produzione'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.Items.Strings = (
              'S'
              'N')
            Width = 74
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Fornitori'
      ImageIndex = 2
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 820
        Height = 595
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          DataController.DataSource = DSTblspp
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Options.Editing = False
          end
          object cxGridDBTableView1cod_spp: TcxGridDBColumn
            DataBinding.FieldName = 'cod_spp'
          end
          object cxGridDBTableView1nam_spp: TcxGridDBColumn
            DataBinding.FieldName = 'nam_spp'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object DSTblPDC: TDataSource
    DataSet = DM.TblPDC
    Left = 702
    Top = 125
  end
  object DSTblREG: TDataSource
    DataSet = DM.TblREG
    Left = 646
    Top = 128
  end
  object DSTblspp: TDataSource
    DataSet = DM.Tblspp
    Left = 644
    Top = 192
  end
end
