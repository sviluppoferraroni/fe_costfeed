unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlue,
  dxSkinVisualStudio2013Blue, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Vcl.ComCtrls, Vcl.ExtCtrls, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxDropDownEdit, Vcl.StdCtrls, dxSkinBlack,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TFmain = class(TForm)
    PG1: TPageControl;
    Panel1: TPanel;
    Tabmain: TTabSheet;
    TabPDC: TTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DSTblPDC: TDataSource;
    cxGrid1DBTableView1mastro: TcxGridDBColumn;
    cxGrid1DBTableView1conto: TcxGridDBColumn;
    cxGrid1DBTableView1sottoconto: TcxGridDBColumn;
    cxGrid1DBTableView1descrizione: TcxGridDBColumn;
    cxGrid1DBTableView1tipo: TcxGridDBColumn;
    cxGrid1DBTableView1fg_produzione: TcxGridDBColumn;
    DSTblREG: TDataSource;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1tipo: TcxGridDBColumn;
    cxGrid2DBTableView1mastro: TcxGridDBColumn;
    cxGrid2DBTableView1conto: TcxGridDBColumn;
    cxGrid2DBTableView1sottoconto: TcxGridDBColumn;
    cxGrid2DBTableView1data: TcxGridDBColumn;
    cxGrid2DBTableView1importo: TcxGridDBColumn;
    Bcespite: TButton;
    cxGrid2DBTableView1descrizione: TcxGridDBColumn;
    cxGrid2DBTableView1protocollo: TcxGridDBColumn;
    TabSheet1: TTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    DSTblspp: TDataSource;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1cod_spp: TcxGridDBColumn;
    cxGridDBTableView1nam_spp: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BcespiteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fmain: TFmain;

implementation

{$R *.dfm}

uses Udet, Udm;

procedure TFmain.BcespiteClick(Sender: TObject);
begin
  fdet.TblREG.Connection            := dm.DB;
  fdet.TblDoc.Connection            := dm.DB;
  fdet.TBlsca.Connection            := dm.DB;
  fdet.TblPDC.Active                := true;
  fdet.TblPDC.EmptyDataSet;
  fdet.TblDoc.Open;
  fdet.TblDoc.Append;
  fdet.TblDocprotocollo.AsString:=  formatdatetime('yymmddhhnnss',now);
  dm.TblPDC.First;
  while not dm.tblpdc.eof do
  begin
    //__>
    DM.applyStyle                   :=  False;
    fdet.TblPDC.append;
    fdet.TblPDCscelta.AsBoolean     := false;
    fdet.TblPDCperc.AsFloat         := 0;
    fdet.TblPDCmastro.asstring      := dm.TblPDCmastro.AsString;
    fdet.TblPDCconto.asstring       := dm.TblPDCconto.AsString;
    fdet.TblPDCsottoconto.asstring  := dm.TblPDCsottoconto.AsString;
    fdet.TblPDCdescrizione.AsString := dm.TblPDCdescrizione.AsString;
    fdet.TblPDCtipo.AsString        := dm.TblPDCtipo.AsString;
    DM.applyStyle                   := True;
    fdet.TblPDc.post;
    dm.tblpdc.Next;
    //__>
  end;
  fdet.TblPDc.first;
  fdet.ShowModal;
  dm.tblreg.Refresh;
end;

procedure TFmain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //

  //
end;

procedure TFmain.FormShow(Sender: TObject);
begin
  //...//
  caption:='Cost Feed Control - 1.00';
  //...//

  Pg1.ActivePage:=Tabmain;
end;

end.
